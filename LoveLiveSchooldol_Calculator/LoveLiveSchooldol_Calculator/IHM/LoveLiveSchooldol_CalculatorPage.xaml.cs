﻿using System;
using Xamarin.Forms;

namespace LoveLiveSchooldol_Calculator
{
	public partial class LoveLiveSchooldol_CalculatorPage : ContentPage
	{
		public LoveLiveSchooldol_CalculatorPage()
		{
			InitializeComponent();
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new LG_Counter());
		}
	}
}
