﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LoveLiveSchooldol_Calculator
{
	public partial class LG_Counter : ContentPage
	{
		public LG_Counter()
		{
			InitializeComponent();
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			List<Anniversaire> annivs = new List<Anniversaire>();
			annivs.Add(new Anniversaire("Nico", new DateTime(1997, 7, 22)));
			annivs.Add(new Anniversaire("Maki", new DateTime(1999, 4, 19)));
			annivs.Add(new Anniversaire("You", new DateTime(1998, 4, 17)));
			annivs.Add(new Anniversaire("Mari", new DateTime(1997, 6, 17)));
			annivs.Add(new Anniversaire("Dia", new DateTime(1997, 1, 1)));
			annivs.Add(new Anniversaire("Hanayo", new DateTime(1999, 1, 17)));
			annivs.Add(new Anniversaire("Honoka", new DateTime(1998, 8, 3)));
			annivs.Add(new Anniversaire("Eli", new DateTime(1997, 10, 21)));
			annivs.Add(new Anniversaire("Kotori", new DateTime(1998, 9, 12)));
			annivs.Add(new Anniversaire("Umi", new DateTime(1998, 3, 15)));
			annivs.Add(new Anniversaire("Rin", new DateTime(1999, 11, 1)));
			annivs.Add(new Anniversaire("Nozomi", new DateTime(1997, 6, 9)));
			annivs.Add(new Anniversaire("Chika", new DateTime(1998, 8, 1)));
			annivs.Add(new Anniversaire("Riko", new DateTime(1998, 9, 19)));
			annivs.Add(new Anniversaire("Kanan", new DateTime(1997, 2, 10)));
			annivs.Add(new Anniversaire("Yoshiko", new DateTime(1999, 7, 13)));
			annivs.Add(new Anniversaire("Hanamaru", new DateTime(1999, 3, 4)));
			annivs.Add(new Anniversaire("Ruby", new DateTime(1999, 9, 21)));

			LGCalculator lg = new LGCalculator(dpDebut.Date, dpFin.Date, annivs);

			DisplayAlert("Nombre de love gems", "Vous gagnerez " + lg.getNbLG() + " love gems en faisant uniquement une partie tous les jours", "OK <3");
		}
	}
}
