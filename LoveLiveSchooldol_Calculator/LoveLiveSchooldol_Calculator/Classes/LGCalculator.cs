﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace LoveLiveSchooldol_Calculator
{
	public class LGCalculator : Calculator
	{
		DateTime dateDebut;
		DateTime dateFin;
		List<Anniversaire> listeBDays;

		public LGCalculator(DateTime dd, DateTime df, List<Anniversaire> annivsLolis)
		{
			this.dateDebut = dd;
			this.dateFin = df;
			this.listeBDays = annivsLolis;
		}

		public int getNbLG()
		{
			//Définition du compteur de LG
			int LGTotal = 0;

			//Trouver le nombre de jours entre les deux dates
			double diffDates = (dateFin - dateDebut).TotalDays + 1;
			int nbJours = (int)diffDates;

			//Ajout au compteur du nombre de jours
			LGTotal += nbJours;

			/* Tous les mois sauf février: 1-6-11-16-21-26-30 +1 LG ces jours là
			 * Février : Pas le 30
			 * 
			 * Anniversaires: +5 LG ces jours là
			 */
			int[] joursValides = { 1, 6, 11, 16, 21, 26, 30 };

			DateTime parcours = new DateTime(dateDebut.Year, dateDebut.Month, dateDebut.Day);

			while (parcours <= dateFin)
			{
				if (joursValides.Contains(parcours.Day))
				{
					LGTotal++;
				}

				foreach (Anniversaire a in listeBDays)
				{
					if (a.anniv.Day.CompareTo(parcours.Day) == 0 && a.anniv.Month.CompareTo(parcours.Month) == 0)
					{
						LGTotal += 5;
					}
				}

				parcours = parcours.AddDays(1);
			}

			//Retour final
			return LGTotal;
		}
	}
}
